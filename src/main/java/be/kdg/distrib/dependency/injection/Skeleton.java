package be.kdg.distrib.dependency.injection;

import ownerszz.libraries.dependency.injection.core.Dependency;
import ownerszz.libraries.dependency.injection.core.DependencyLifecycle;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Dependency(runnable = true, lifecycle = DependencyLifecycle.SINGLETON)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Skeleton {
    int port() default 0;
}
