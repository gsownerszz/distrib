package be.kdg.distrib.dependency.injection;

import be.kdg.distrib.skeletonFactory.SkeletonFactory;
import be.kdg.distrib.stubFactory.StubFactory;
import ownerszz.libraries.dependency.injection.annotation.scanner.AnnotationScanner;
import ownerszz.libraries.dependency.injection.core.DependencyManager;
import ownerszz.libraries.dependency.injection.core.DependencyRegistrator;
import ownerszz.libraries.dependency.injection.core.ResolveDependencies;

@DependencyRegistrator
public class DistribRegistrator {
    @ResolveDependencies
    public DistribRegistrator(DependencyManager dependencyManager) throws Exception {
        dependencyManager.registerPoxyOnAnnotation(Skeleton.class, impl -> {
            Skeleton ann = AnnotationScanner.getAnnotation(impl.getClass(),Skeleton.class);
            return SkeletonFactory.createSkeleton(impl, ann.port());
        });
        dependencyManager.registerPoxyOnAnnotation(Stub.class, interfaze ->{
            Class interfazeClass = interfaze.getClass().getInterfaces()[0];
            Stub ann = AnnotationScanner.getAnnotation(interfazeClass, Stub.class);
            if(ann.resultAddress().equals("") && ann.resultClass() == Object.class){
                return StubFactory.createStub(interfazeClass, ann.skeletonAddress(), ann.skeletonPort());
            }else {
                if (ann.resultClass() != Object.class){
                    try {
                        be.kdg.distrib.skeletonFactory.Skeleton result = (be.kdg.distrib.skeletonFactory.Skeleton) DependencyManager.createInstance(ann.resultClass());
                        return StubFactory.createStub(interfazeClass,
                                ann.skeletonAddress(),
                                ann.skeletonPort(),
                                result.getAddress().getIpAddress(),
                                result.getAddress().getPortNumber());
                    } catch (Throwable e) {
                        e.printStackTrace();
                        return null;
                    }
                }else {
                    return StubFactory.createStub(interfazeClass,
                            ann.skeletonAddress(),
                            ann.skeletonPort(),
                            ann.resultAddress(),
                            ann.resultPort());

                }
            }
        } );
    }
}
