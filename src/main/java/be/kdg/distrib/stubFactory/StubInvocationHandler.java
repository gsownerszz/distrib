package be.kdg.distrib.stubFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;
import be.kdg.distrib.reflection_tools.ArgumentCreator;
import be.kdg.distrib.reflection_tools.ParameterCreator;


import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

public class StubInvocationHandler implements InvocationHandler {
    private final MessageManager messageManager;
    private final NetworkAddress skeletonAddress;
    private NetworkAddress resultAddress;
    public StubInvocationHandler(NetworkAddress skeletonAddress) {
        this.skeletonAddress = skeletonAddress;
        this.messageManager = new MessageManager();
    }
    public StubInvocationHandler(NetworkAddress skeletonAddress, NetworkAddress resultAddress) {
        this(skeletonAddress);
        this.resultAddress = resultAddress;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        try {
           sendRequest(buildRequest(method, args));
           return checkReply(method,messageManager.wReceive());
        }catch (Exception e){
           throw new RuntimeException(e.getMessage());
        }
    }
    private MethodCallMessage buildRequest(Method method, Object[] args) throws IllegalAccessException {
        if(resultAddress != null){
            return buildRequest(method, resultAddress, args);
        }else {
            String methodName = method.getName();
            ArgumentCreator argumentCreator = new ArgumentCreator();
            Map<String, String> argumentList = argumentCreator.createArgumentList(method,args);
            return new MethodCallMessage(messageManager.getMyAddress(),methodName,argumentList);
        }
    }
    private MethodCallMessage buildRequest(Method method, NetworkAddress resultAddress,Object[] args) throws IllegalAccessException {
        String methodName = method.getName();
        ArgumentCreator argumentCreator = new ArgumentCreator();
        Map<String, String> argumentList = argumentCreator.createArgumentList(method,resultAddress.toString(),args);
        return new MethodCallMessage(messageManager.getMyAddress(),methodName,argumentList);
    }
    private void sendRequest(MethodCallMessage request){
        messageManager.send(request,skeletonAddress);
    }
    private Object checkReply(Method method,MethodCallMessage reply) throws Exception {
        if (method.getReturnType().equals(Void.TYPE)){
            //Check Ok
            if ("Ok".equals(reply.getParameter("result"))){
                return null;
            }else {
                throw new RuntimeException(reply.getParameter("reason"));
            }
        }else {
            if ("!Ok".equals(reply.getParameter("result"))){
                throw new RuntimeException(reply.getParameter("reason"));
            }else {
                ParameterCreator parameterCreator = new ParameterCreator();
                return parameterCreator.createParameterList(new Class[]{method.getReturnType()},reply)[0];

            }
        }
    }
}
