package be.kdg.distrib.stubFactory;

import be.kdg.distrib.communication.NetworkAddress;
import be.kdg.distrib.skeletonFactory.Skeleton;
import be.kdg.distrib.skeletonFactory.SkeletonInvocationHandler;

import java.lang.reflect.Proxy;

public class StubFactory {
    public static Object createStub(Class interfaceToStub, String skeletonAddress, int port) {
        NetworkAddress networkAddress = new NetworkAddress(skeletonAddress,port);
        StubInvocationHandler handler = new StubInvocationHandler(networkAddress);
        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{interfaceToStub}, handler);
    }
    public static Object createStub(Class interfaceToStub, String skeletonAddress, int port,String resultAddress, int resultPort) {
        NetworkAddress skeleton = new NetworkAddress(skeletonAddress,port);
        NetworkAddress result =  new NetworkAddress(resultAddress, resultPort);
        StubInvocationHandler handler = new StubInvocationHandler(skeleton, result);
        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{interfaceToStub}, handler);
    }
}
