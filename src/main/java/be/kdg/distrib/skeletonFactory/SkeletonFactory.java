package be.kdg.distrib.skeletonFactory;

import java.lang.reflect.Proxy;

public class SkeletonFactory {
    public static Object createSkeleton(Object object)  {
           return createSkeleton(object,0);
    }
    public static Object createSkeleton(Object object, int port)  {
        SkeletonInvocationHandler handler = new SkeletonInvocationHandler(object, port);
        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{Skeleton.class}, handler);
    }
}
