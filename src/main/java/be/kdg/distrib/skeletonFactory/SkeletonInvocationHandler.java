package be.kdg.distrib.skeletonFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;
import be.kdg.distrib.reflection_tools.*;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

public class SkeletonInvocationHandler implements InvocationHandler {
    //The message manager
    private final MessageManager messageManager;
    //List of all the methods that we need to handle
    private final HashSet<Method> implementationMethods;
    //The actual implementation of the business logic.
    private final Object implementation;

    public SkeletonInvocationHandler(Object implementation,int port) {
        this.implementation = implementation;
        this.implementationMethods = new HashSet<>();
        Arrays.stream(implementation.getClass().getInterfaces())
                .map(clazz -> clazz.getMethods())
                .forEach(methods ->  implementationMethods.addAll(Arrays.asList(methods)));
        implementationMethods.addAll(Arrays.asList(implementation.getClass().getMethods()));

        this.messageManager = new MessageManager(port);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //Get the method name
        String methodName = method.getName();
        if ("run".equals(methodName)) {
            //Creates and starts a thread that will listen to all incoming requests
            new Thread(() -> {
                //Show our address
                System.out.println(messageManager.getMyAddress());
                //Await messages
                while (true) {
                    //Try catch this so that our skeleton doesn't crash
                    try {
                        MethodCallMessage request = messageManager.wReceive();
                        handleRequest(implementation, request);
                    }catch (Exception ignored){

                    }

                }
            }).start();
        } else if ("getAddress".equals(methodName)) {
            //Return our address
            return messageManager.getMyAddress();
        } else if ("handleRequest".equals(methodName)) {
            //Handle the request
            handleRequest(implementation, (MethodCallMessage) args[0]);
        }
        //always return null here. This result will/may not be used.
        return null;
    }

    private void handleRequest(Object skeletonImplementation, MethodCallMessage request) {
        try {
            MethodFinder methodFinder = new MethodFinder();
            //Find the method
            Method toInvoke = methodFinder.findMethod(implementationMethods,request);
                /*Create a method reflection that will do the following things: - Check if parameter count and (distinct) argument count match.
                                                                                - Create the parameters if there are any.
                */
                MethodReflection methodReflection = new MethodReflection();
                MethodReflectionResult methodReflectionResult =  methodReflection.createReflectionResult(toInvoke,request);
            if (Arrays.stream(Object.class.getMethods()).anyMatch(e -> e.getName().equals(toInvoke.getName()))
                    && methodReflectionResult.getReturnType().equals(Void.TYPE)){
                    sendOkReply(request);
                    return;
            }else if (request.getMethodName().equals("getClass")){
                MethodCallMessage result = new MethodCallMessage(messageManager.getMyAddress(), "getClass");
                result.setParameter("result", "null");
                sendReply(result, request.getOriginator());
                return;
            }
                //check if the method is void using the MethodReflectionResult.
                if (methodReflectionResult.getReturnType().equals(Void.TYPE)){
                    //check if the method has any parameters using the MethodReflectionResult
                    if (methodReflectionResult.hasParameters()){
                        //If the method has parameters then invoke the method with the parameters from MethodReflectionResult
                        toInvoke.invoke(skeletonImplementation,methodReflectionResult.getParameters());
                    }else {
                        //If the method has no parameters just invoke it
                        toInvoke.invoke(skeletonImplementation);
                    }
                    //Send an Ok reply to tell the originator that the request was handled successfully
                    sendOkReply(request);
                }else {
                    //Prepare a result
                    MethodCallMessage result;
                    //check if the method has any parameters using the MethodReflectionResult
                    if (methodReflectionResult.hasParameters()){
                        /*If the method has parameters then invoke the method with the parameters from MethodReflectionResult
                            We will then build the response using the result of the invocation
                        */
                        result = buildResponse(toInvoke.invoke(skeletonImplementation,methodReflectionResult.getParameters()),request.getMethodName());
                    }else {
                        /*If the method has no parameters just invoke it
                            We will then build the response using the result of the invocation
                        */
                        result = buildResponse(toInvoke.invoke(skeletonImplementation),request.getMethodName());
                    }
                    //Send the response to the originator
                    sendReply(result,request.getOriginator());
                }

        } catch (Exception e) {
            //If anything bad happens we will send a !Ok. This is so that the originator knows what's up
            sendNotOkReply(e,request);
            throw new RuntimeException(e.getMessage());
        }
    }

   //Gets only called when a method doesn't return void
    private MethodCallMessage buildResponse(Object invocationResult, String methodName) throws IllegalAccessException {
        //Create result with our address and the incoming methodName
        if (invocationResult == null){
            invocationResult = "null";
        }
        MethodCallMessage result = new MethodCallMessage(messageManager.getMyAddress(), methodName);
        ArgumentCreator argumentCreator = new ArgumentCreator();
        argumentCreator.createResultList(invocationResult,result.getParameters());
        return result;
    }

    private void sendReply(MethodCallMessage result, NetworkAddress requestSender) {
        //Send a actual reply
        messageManager.send(result, requestSender);
    }

    private void sendOkReply(MethodCallMessage request) {
        //Sends a Ok reply to the request maker.
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", "Ok");
        messageManager.send(reply, request.getOriginator());
    }

    private void sendNotOkReply(Exception e,MethodCallMessage request) {
        //Sends a !Ok reply to the request maker.
        //Sends the reason why we !Ok the request.
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", "!Ok");
        reply.setParameter("reason",e.getMessage()) ;
        messageManager.send(reply, request.getOriginator());
    }

}
