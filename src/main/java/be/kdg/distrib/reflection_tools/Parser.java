package be.kdg.distrib.reflection_tools;

public class Parser {
    public Object parseValue(Class type, String value) {
        if (value == null){
            return "";
        }
        if (isInteger(type)) {
            return (Integer.valueOf(value));
        } else if (isByte(type)) {
            return (Byte.valueOf(value));
        } else if (isDouble(type)) {
            return (Double.valueOf(value));
        } else if (isShort(type)) {
            return (Short.valueOf(value));
        } else if (isFloat(type)) {
            return (Float.valueOf(value));
        } else if (isLong(type)) {
            return (Long.valueOf(value));
        } else if (isBoolean(type)) {
            //The check in Boolean.valueOf() is weak.
            // It will take any other argument and will try to equal it with "true" resulting in always getting a boolean value even when the argument is not a boolean.
            if (value.equalsIgnoreCase("true") ||value.equalsIgnoreCase("false")){
                return (Boolean.valueOf(value));
            }
            throw new ClassCastException("Was not given an boolean");
        } else if (isCharacter(type)) {
            if (value.length() > 1){
                throw new ClassCastException("Was not given an char");
            }
            return (Character.valueOf(value.charAt(0)));
        } else if (isString(type)) {
            if (type.getSimpleName().equalsIgnoreCase("stringbuilder")){
                return new StringBuilder(value);
            }
            return (String.valueOf(value));
        } else {
            throw new RuntimeException("Nested classes not supported");
        }
    }
    private boolean isCharacter(Class clazz) {
        return getClassName(clazz).equalsIgnoreCase("character") || getClassName(clazz).equalsIgnoreCase("char");
    }

    private boolean isString(Class clazz) {
        return getClassName(clazz).equalsIgnoreCase("string") || getClassName(clazz).equalsIgnoreCase("stringbuilder");
    }

    private boolean isBoolean(Class clazz) {
        return getClassName(clazz).equalsIgnoreCase("boolean");
    }

    private boolean isDouble(Class clazz) {
        return getClassName(clazz).equalsIgnoreCase("double");
    }

    private boolean isFloat(Class clazz) {
        return getClassName(clazz).equalsIgnoreCase("float");
    }

    private boolean isByte(Class clazz) {
        return getClassName(clazz).equalsIgnoreCase("byte");
    }

    private boolean isShort(Class clazz) {
        return getClassName(clazz).equalsIgnoreCase("short");
    }

    private boolean isLong(Class clazz) {
        return getClassName(clazz).equalsIgnoreCase("long");
    }

    private boolean isInteger(Class clazz) {
        return getClassName(clazz).equalsIgnoreCase("integer") || getClassName(clazz).equalsIgnoreCase("int");
    }

    public boolean isStringOrPrimitive(Class clazz) {
        return isPrimitiveType(clazz) || isString(clazz);
    }

    private boolean isPrimitiveType(Class clazz) {
        return clazz.isPrimitive()
                || isCharacter(clazz)
                || isBoolean(clazz)
                || isLong(clazz)
                || isFloat(clazz)
                || isDouble(clazz)
                || isByte(clazz)
                || isInteger(clazz)
                || isShort(clazz);

    }
    private String getClassName(Class clazz){
        return clazz.getSimpleName();
    }

}
