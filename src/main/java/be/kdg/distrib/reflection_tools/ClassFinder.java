package be.kdg.distrib.reflection_tools;
import be.kdg.distrib.annotation.IgnoreField;


import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

public class ClassFinder {
    Class findBestInterfaceImplementation(Class interfaceClazz, Map<String, String> minumumFields) throws ClassNotFoundException {
        Optional<Class> foundImpl = ClassScanner.getMatchingClasses(interfaceClazz).stream().filter(clazz -> isBestImplementation(clazz, minumumFields)).findFirst();
        if (foundImpl.isPresent()){
            return foundImpl.get();
        }else {
            throw new ClassNotFoundException("No implementation of: " + interfaceClazz.getSimpleName() + " found");
        }
    }
    private boolean isBestImplementation(Class clazz, Map<String, String> expectedFields){
        ArrayList<String> fieldNames = (ArrayList<String>) expectedFields.keySet().stream().map(argname -> argname.split("\\.")[1]).collect(Collectors.toList());
        if (clazz.getDeclaredFields().length == 0){
            return false;
        }
        for (Field f : clazz.getDeclaredFields()) {
            boolean matched;
            if (f.isAnnotationPresent(IgnoreField.class)){
                matched = !fieldNames.contains(f.getName());
            }else {
                matched = fieldNames.contains(f.getName());
            }

            if (!matched) {
                return false;
            }
        }
        return true;
    }
}
