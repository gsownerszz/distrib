package be.kdg.distrib.reflection_tools;

import be.kdg.distrib.annotation.CallByRef;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.stubFactory.StubFactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class ParameterCreator {
    private final int MAX_RECURSIVE_OBJECT = GlobalSettings.MAX_RECURSIVE_OBJECT;
    //Isolating specific fields to make the for-each in assignValues faster
    private Map<String,String> getObjectFields(Map<String, String> parameters,String s){
            Map<String, String> result = new HashMap<>();
            for(String key : parameters.keySet()) {
                if (key.startsWith(s)) {
                    String value = parameters.get(key);
                    result.put(key, value);
                }
            }
            return result;
    }
    private void assignValuesToObject(int depth,TreeSet<String> doneFields ,Object object, Map<String,String> fields) throws NoSuchFieldException, IllegalAccessException, InvocationTargetException, InstantiationException{
        if (depth > MAX_RECURSIVE_OBJECT){
            throw new RuntimeException("Maximum recursive object reached.");
        }
        Parser parser = new Parser();
        //Get the field from the Map
        for (Iterator<String> it = fields.keySet().iterator(); it.hasNext(); ) {
            String field = it.next();
            //Check if we already did this field
            if (!doneFields.contains(field)){
                /*Get the actual name of the field
                 * Example:  field = arg0.name
                 *           but we want "name"
                 *           result -> actualField = name
                 */
                String actualField = field.split("\\.")[depth];
                //Look the field up. If it doesn't exist it will throw a NoSuchFieldException
                Class currentObjectClass = object.getClass();
                Field[] myFields = currentObjectClass.getDeclaredFields();
                Field f = object.getClass().getDeclaredField(actualField);
                //Make sure the Field is not null
                if (f != null) {
                    //Make it possible to get and set its value
                    f.setAccessible(true);
                    //Get its type
                    Class type = f.getType();
                    if (parser.isStringOrPrimitive(type)){
                        //Get the value from the Map
                        String argument = fields.get(field);
                        //Make sure we can convert
                        f.set(object,parser.parseValue(type,argument));

                    }else {
                        if (fields.get(field) == null || "null".equals(fields.get(field))){
                            f.set(object, null);
                        }else {
                            //Find the default constructor of the field
                            DefaultConstructorFinder constructorFinder = new DefaultConstructorFinder();
                            Constructor constructor = constructorFinder.getDefaultContructor(type.getDeclaredConstructors());
                            //Create an instance of the field
                            Object fieldObject = constructor.newInstance();
                            String startWith = field.split(actualField)[0] + actualField;
                            Map<String,String> objectFields = getObjectFields(fields,startWith);
                            //recursively create the current object
                            assignValuesToObject(depth + 1, doneFields,fieldObject, objectFields);
                            //Set the field value to the created instance
                            f.set(object,fieldObject);
                        }

                    }
                    //Add the field to the doneFields TreeSet.
                    doneFields.add(field);
                    //Remove the current field from the iteration.
                    it.remove();
                }
            }

        }

    }
    private void assignValuesToObject(Object object, Map<String, String> fields) throws NoSuchFieldException, IllegalAccessException, InvocationTargetException, InstantiationException {
        //Start creating the object recursively
        assignValuesToObject(1,new TreeSet<>(),object,fields);
    }
    public Object[] createParameterList(Method method, MethodCallMessage request) throws Exception {
        /* Explanation of variabeles:   - parameters is an array that will contain the parameters for the method invocation.
         *                               - argumentNames is an array that will contain all the names of arguments that come with the request
         *                               - arguments is an array that will contain all the values of arguments that come with the request
         *                               - doneArguments will contain all the argumentNames split by "." if possible so that we get only unique arguments
         *                               - argumentCounter will count at what argument number we are. We use this to fill up parameters and also to get the type of the parameter
         *                               - i will count at which iteration we are. We use this to get the values from the arguments array
         */
        Class[] parameterTypes = method.getParameterTypes();
        Object[] parameters = new Object[parameterTypes.length];
        Object[] argumentNames = request.getParameters().keySet().toArray();
        Object[] arguments = request.getParameters().values().toArray();
        TreeSet<String> doneArguments = new TreeSet<>();
        Parser parser = new Parser();
        int argumentCounter = 0;
        int i = 0;
        //Make sure all arguments are there.
        while (argumentCounter < parameterTypes.length) {
            //Split on "."
            String arg = argumentNames[i].toString().split("\\.")[0] += ".";
            //Check if we already created this object
            if (!doneArguments.contains(arg)) {
                //Get the type of the current parameter
                Class type = parameterTypes[argumentCounter];

                //Check if parameter is a primitive or string
                if (parser.isStringOrPrimitive(type)) {
                    String argument = arguments[i].toString();
                    parameters[argumentCounter] = parser.parseValue(type,argument);
                    doneArguments.add(argument);
                    argumentCounter++;
                }
                else if (method.getParameters()[i].isAnnotationPresent(CallByRef.class)){
                    String argument = arguments[i].toString();
                    String ipAddress = argument.split(":")[0];
                    int port = Integer.parseInt(argument.split(":")[1]);
                    parameters[argumentCounter] = StubFactory.createStub(type,ipAddress, port);
                    argumentCounter++;
                }
                else {
                    //Is a custom object
                    if (arguments[i] == null || "null".equals(arguments[i].toString())){
                        parameters[argumentCounter] = null;
                        argumentCounter++;
                    }else{
                        if (type.isInterface()){
                            ClassFinder classFinder = new ClassFinder();
                            type = classFinder.findBestInterfaceImplementation(type, getObjectFields(request.getParameters(),arg));
                        }
                        Map<String, String> objectFields = getObjectFields(request.getParameters(),arg);
                        DefaultConstructorFinder defaultConstructorFinder = new DefaultConstructorFinder();
                        Constructor defaultConstructor = defaultConstructorFinder.getDefaultContructor(type.getDeclaredConstructors());
                        Object obj = defaultConstructor.newInstance();
                        try {
                            assignValuesToObject(obj, objectFields);
                        } catch (Exception e) {
                            throw new RuntimeException(e.getMessage());
                        }
                        parameters[argumentCounter] = obj;
                        doneArguments.add(arg);
                        argumentCounter++;
                    }
                }
            }
            i++;
        }

        return parameters;
    }
    public Object[] createParameterList(Class[] parameterTypes, MethodCallMessage request) throws Exception {
        /* Explanation of variabeles:   - parameters is an array that will contain the parameters for the method invocation.
         *                               - argumentNames is an array that will contain all the names of arguments that come with the request
         *                               - arguments is an array that will contain all the values of arguments that come with the request
         *                               - doneArguments will contain all the argumentNames split by "." if possible so that we get only unique arguments
         *                               - argumentCounter will count at what argument number we are. We use this to fill up parameters and also to get the type of the parameter
         *                               - i will count at which iteration we are. We use this to get the values from the arguments array
         */
        Object[] parameters = new Object[parameterTypes.length];
        Object[] argumentNames = request.getParameters().keySet().toArray();
        Object[] arguments = request.getParameters().values().toArray();
        TreeSet<String> doneArguments = new TreeSet<>();
        Parser parser = new Parser();
        int argumentCounter = 0;
        int i = 0;
        //Make sure all arguments are there.
        while (argumentCounter < parameterTypes.length) {
            //Split on "."
            String arg = argumentNames[i].toString().split("\\.")[0] += ".";
            //Check if we already created this object
            if (!doneArguments.contains(arg)) {
                //Get the type of the current parameter
                Class type = parameterTypes[argumentCounter];
                //Check if parameter is a primitive or string
                if (parser.isStringOrPrimitive(type)) {
                    String argument = arguments[i].toString();
                    parameters[argumentCounter] = parser.parseValue(type,argument);
                    doneArguments.add(argument);
                    argumentCounter++;
                }
                else if (type.isAnnotationPresent(CallByRef.class)){
                    String argument = arguments[i].toString();
                    String ipAddress = argument.split(":")[0];
                    int port = Integer.parseInt(argument.split(":")[1]);
                    parameters[argumentCounter] = StubFactory.createStub(type,ipAddress, port);
                    argumentCounter++;
                }
                else {
                    if (type.isInterface()){
                        ClassFinder classFinder = new ClassFinder();
                        type = classFinder.findBestInterfaceImplementation(type, getObjectFields(request.getParameters(),arg));
                    }
                    //Is a custom object
                    Map<String, String> objectFields = getObjectFields(request.getParameters(),arg);
                    DefaultConstructorFinder defaultConstructorFinder = new DefaultConstructorFinder();
                    Constructor defaultConstructor = defaultConstructorFinder.getDefaultContructor(type.getDeclaredConstructors());
                    Object obj = defaultConstructor.newInstance();
                    try {
                        assignValuesToObject(obj, objectFields);
                    } catch (Exception e) {
                        throw new RuntimeException(e.getMessage());
                    }
                    parameters[argumentCounter] = obj;
                    doneArguments.add(arg);
                    argumentCounter++;
                }
            }
            i++;
        }

        return parameters;
    }

}
