package be.kdg.distrib.reflection_tools;

import be.kdg.distrib.annotation.CallByRef;
import be.kdg.distrib.communication.MethodCallMessage;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.TreeSet;

public class MethodFinder {
    //Will return true  if the amount of arguments match and that the parameter types match.
    private boolean isRightMethod(Method method, MethodCallMessage request) {
        if (method.getName().equals(request.getMethodName())) {
            ArgumentCounter argumentCounter = new ArgumentCounter();
            if (method.getParameterCount() != argumentCounter.getDistinctArgumentCount(request)) {
                return false;
            }
            if (!isSameClasses(method, request)) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    //Will return true if the parameter types match the signature of the request
    private boolean isSameClasses(Method method, MethodCallMessage request) {
        Parser parser = new Parser();
        TreeSet<String> doneArguments = new TreeSet<>();
        for (int i = 0; i < method.getParameterTypes().length; i++) {
            //get the required type
            Class type = method.getParameterTypes()[i];
            //get the argument key ex. arg0, arg1.field1
            String key = request.getParameters().keySet().toArray()[i].toString();
            //check if the key contains a "." and will split it to only take the part from index 0 until the dot and then add a dot.
            if (key.contains(".")) {
                key = key.split("\\.")[0] + ".";
            }
            if (!doneArguments.contains(key)) {
                //Get the value
                String value = request.getParameters().values().toArray()[i].toString();
                //Check if the required type is a String or primitive
                if (parser.isStringOrPrimitive(type)) {
                    //Check if the argument is a custom object
                    if (key.contains(".")) {
                        return false;
                    }
                    //Try to create the value. This will fail if the value can't be created into the signature
                    try {
                        parser.parseValue(type, value);
                        doneArguments.add(key);
                    } catch (Exception e) {
                        return false;
                    }
                }
                else if(method.getParameters()[i].isAnnotationPresent(CallByRef.class)){
                    doneArguments.add(key);
                }
                else {
                    //Check if the argument is not a custom object as required
                    if (!key.contains(".") && !"null".equals(value)) {
                        return false;
                    }else {
                        //Will check that we are talking about the same class

                        for (Field f : type.getDeclaredFields()) {
                            boolean matched = false;
                            if (f.getDeclaringClass() == type) {
                                matched = true;
                            }
                            if (!matched) {
                                return false;
                            }
                        }
                    }

                    doneArguments.add(key);
                }
            }

        }
        return true;
    }

    public Method findMethod(HashSet<Method> methods, MethodCallMessage request) {
        Optional<Method> method = methods.stream().filter(m -> isRightMethod(m, request)).findFirst();
        if (method.isPresent()) {
            return method.get();
        } else {
            throw new RuntimeException("Method: " + request.getMethodName() + " not found");
        }
    }
}

