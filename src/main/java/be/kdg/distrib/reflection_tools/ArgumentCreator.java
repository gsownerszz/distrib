package be.kdg.distrib.reflection_tools;

import be.kdg.distrib.annotation.CallByRef;
import be.kdg.distrib.annotation.IgnoreField;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.Map;


public class ArgumentCreator {
    private final int MAX_RECURSIVE_OBJECT = GlobalSettings.MAX_RECURSIVE_OBJECT;

    public Map<String, String> createArgumentList(Class[] parameterTypes, Object[] args) throws IllegalAccessException {
        HashMap<String, String> arguments = new HashMap<>();
        int argumentCounter = 0;
        String argumentPrefix = "arg";
        Parser parser = new Parser();
        for (Class clazz : parameterTypes) {
            String argName = argumentPrefix + argumentCounter;
            if (parser.isStringOrPrimitive(clazz)) {
                arguments.putIfAbsent(argName, args[argumentCounter].toString());
            } else {
                createObjectArguments(args[argumentCounter], arguments, argName);
            }
            argumentCounter++;
        }
        return arguments;
    }

    public Map<String, String> createArgumentList(Method method, Object[] args) throws IllegalAccessException {
        HashMap<String, String> arguments = new HashMap<>();
        int argumentCounter = 0;
        String argumentPrefix = "arg";
        Parser parser = new Parser();
        Class[] parameterTypes = method.getParameterTypes();

        for (Class clazz : parameterTypes) {
            String argName = argumentPrefix + argumentCounter;
            if (parser.isStringOrPrimitive(clazz)) {
                arguments.putIfAbsent(argName, args[argumentCounter].toString());
            } else {
                createObjectArguments(args[argumentCounter], arguments, argName);
            }
            argumentCounter++;
        }
        return arguments;
    }

    public Map<String, String> createArgumentList(Method method, String resultAddress, Object[] args) throws IllegalAccessException {
        HashMap<String, String> arguments = new HashMap<>();
        int argumentCounter = 0;
        String argumentPrefix = "arg";
        Parser parser = new Parser();
        for (Parameter parameter : method.getParameters()) {
            Class clazz = parameter.getType();
            String argName = argumentPrefix + argumentCounter;
            if (parser.isStringOrPrimitive(clazz)) {
                if (args[argumentCounter] == null) {
                    arguments.putIfAbsent(argName, "null");
                } else {
                    arguments.putIfAbsent(argName, args[argumentCounter].toString());
                }
            } else if (parameter.isAnnotationPresent(CallByRef.class)) {
                arguments.putIfAbsent(argName, resultAddress);
            } else {
                createObjectArguments(args[argumentCounter], arguments, argName);
            }
            argumentCounter++;
        }
        return arguments;
    }


    private void createObjectArguments(Object object, Map<String, String> arguments, String prefix) throws IllegalAccessException {
        createObjectArguments(1, object, arguments, prefix);
    }

    private void createObjectArguments(int depth, Object object, Map<String, String> arguments, String prefix) throws IllegalAccessException {
        if (depth > MAX_RECURSIVE_OBJECT) {
            throw new RuntimeException("Maximum recursive object reached.");
        }
        if (object == null) {
            arguments.putIfAbsent(prefix, "null");
        } else {
            Parser parser = new Parser();
            Field[] objectFields = object.getClass().getDeclaredFields();
            for (Field field : objectFields) {
                if (!field.isAnnotationPresent(IgnoreField.class)){
                    field.setAccessible(true);
                    String argumentName = prefix + "." + field.getName();
                    if (parser.isStringOrPrimitive(field.getType())) {
                        if (field.get(object) == null) {
                            arguments.putIfAbsent(argumentName, "null");

                        } else {
                            arguments.putIfAbsent(argumentName, field.get(object).toString());
                        }
                    } else {
                        depth++;
                        createObjectArguments(depth, field.get(object), arguments, argumentName);
                    }
                }
            }
        }

    }

    public void createResultList(Object resultObject, Map<String, String> argumentList) throws IllegalAccessException {
        Parser parser = new Parser();
        String prefix = "result";
        if (parser.isStringOrPrimitive(resultObject.getClass())) {
            //If the return type is primitive or a String we return a simple result.
            argumentList.putIfAbsent(prefix, resultObject.toString());
        } else {
            //If the return type is a custom class we will return a result and all parameters follow this template: result.<field name of custom class>
            createObjectArguments(resultObject, argumentList, prefix);
        }
    }
}
