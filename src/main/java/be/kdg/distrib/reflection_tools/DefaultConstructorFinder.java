package be.kdg.distrib.reflection_tools;

import java.lang.reflect.Constructor;

public class DefaultConstructorFinder {
    public Constructor getDefaultContructor(Constructor[] constructors){
        for (Constructor constructor: constructors) {
            if (constructor.getParameterCount() == 0){
                return constructor;
            }
        }
        throw new RuntimeException("No default constructor found.");
    }
}
