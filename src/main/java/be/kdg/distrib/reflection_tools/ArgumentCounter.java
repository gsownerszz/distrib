package be.kdg.distrib.reflection_tools;

import be.kdg.distrib.communication.MethodCallMessage;

import java.util.TreeSet;

public class ArgumentCounter {
    public int getDistinctArgumentCount(MethodCallMessage request){
        TreeSet<String> doneArguments = new TreeSet<>();
        Object[] argumentNames = request.getParameters().keySet().toArray();
        for (int i = 0; i < request.getParameters().keySet().size() ; i++) {
            String arg = argumentNames[i].toString().split("\\.")[0] += ".";
            doneArguments.add(arg);
        }
        return doneArguments.size();
    }


}
