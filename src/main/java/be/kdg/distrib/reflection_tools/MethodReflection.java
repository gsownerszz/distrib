package be.kdg.distrib.reflection_tools;

import be.kdg.distrib.communication.MethodCallMessage;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MethodReflection {
    public MethodReflectionResult createReflectionResult(Method method, MethodCallMessage request) throws Exception {
        //Request distinct argument count = parameter count
        ParameterCreator parameterCreator = new ParameterCreator();
        MethodReflectionResult result = null;
        if (method.getParameterCount() == 0 && request.getParameters().values().size() > 0) {
            throw new RuntimeException("Expected 0 arguments");
        }else if (method.getParameterCount() > 0 && request.getParameters().values().size() > 0){
            ArgumentCounter argumentCounter = new ArgumentCounter();
            int argumentCount = argumentCounter.getDistinctArgumentCount(request);
            if (method.getParameterCount() != argumentCount){
                throw new RuntimeException("Amount of arguments don't match \n Expected: " + method.getParameterCount() + " \t Actual: " + argumentCount);
            }
            Object[] parameters = parameterCreator.createParameterList(method, request);
            result = new MethodReflectionResult(method.getReturnType(),parameters,true);
        }
        //The regex for our arguments
        String parameterRegexp = "arg([0-9]+).*";
        //Make sure all request arguments match the regex
        for (String key : request.getParameters().keySet()) {
            if (!key.matches(parameterRegexp)) {
                throw new RuntimeException("Arguments to method calls must follow the following regex: " + parameterRegexp + " \n Example: arg0");
            }
        }
        if (result == null){
            result = new MethodReflectionResult(method.getReturnType(),null,false);
        }
        //return the MethodReflectionResult
        return result;
    }
}
