package be.kdg.distrib.reflection_tools;

public class MethodReflectionResult {
    private final Object[] parameters;
    private final boolean hasParameters;
    private final Class returnType;

    public MethodReflectionResult(Class returnType,Object[] parameters, boolean hasParameters) {
        this.returnType = returnType;
        this.parameters = parameters;
        this.hasParameters = hasParameters;
    }

    public Object[] getParameters() {
        return parameters;
    }

    public boolean hasParameters() {
        return hasParameters;
    }

    public Class getReturnType() {
        return returnType;
    }
}
