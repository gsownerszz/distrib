package be.kdg.distrib.testclasses;

public class TestInterFace4Impl implements TestInterface4 {
    private int i;
    private boolean b;
    private char c;
    private TestObject testObject;
    private TestRecursiveObject1 testRecursiveObject1;
    private TestInterface2 testInterface2;

    public TestInterFace4Impl() {
        b = false;
    }

    @Override
    public void changeFields() {
        i = 123;
        b = true;
        c = 'c';
        testObject = new TestObject("test", i, c, b);
    }

    public void changeFields(int i){
        this.i = i;
        b = true;
        c = 'c';
        testObject = new TestObject("test", i, c, b);
    }

    @Override
    public void changeFields(int i, boolean b) {
        this.i = i;
        this.b = b;
        c = 'c';
        testObject = new TestObject("test", i, c, b);
    }

    @Override
    public void changeFields(int i, boolean b, char c) {
        this.i = i;
        this.b = b;
        this.c = c;
        testObject = new TestObject("test", i, c, b);
    }

    @Override
    public void changeFields(int i, boolean b, char c, TestObject testObject) {
        this.i = i;
        this.b = b;
        this.c = c;
        this.testObject = testObject;
    }

    @Override
    public void trySameNumberOfParametersButOtherTypes(int i, boolean b) {
        this.i = i;
        this.b = b;
    }

    @Override
    public void trySameNumberOfParametersButOtherTypes(int i, TestObject testObject) {
        this.i = i;
        this.testObject = testObject;
    }

    @Override
    public void trySameNumberOfParametersButOtherTypes(int i, char c) {
        this.i = i;
        this.c = c;
    }

    @Override
    public void tryRecursiveParameterObjects(TestRecursiveObject1 recursiveObject) {
        this.testRecursiveObject1 = recursiveObject;
    }

    @Override
    public TestRecursiveObject1 tryRecursiveReturnType() {
        return testRecursiveObject1;
    }

    @Override
    public void tryInterfaceAsParameter(TestInterface2 interface2) {
        testInterface2 = interface2;
    }

    public int getI() {
        return i;
    }

    public boolean isB() {
        return b;
    }

    public char getC() {
        return c;
    }

    public TestObject getTestObject() {
        return testObject;
    }

    public TestRecursiveObject1 getTestRecursiveObject1() {
        return testRecursiveObject1;
    }

    public TestInterface2 getTestInterface2() {
        return testInterface2;
    }
}
