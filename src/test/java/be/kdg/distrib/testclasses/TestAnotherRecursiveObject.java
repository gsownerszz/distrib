package be.kdg.distrib.testclasses;

public class TestAnotherRecursiveObject {
    private TestObject testObject;
    public TestAnotherRecursiveObject(){

    }

    public void setTestObject(TestObject object){
        this.testObject = object;
    }
    public TestObject getTestObject() {
        return testObject;
    }
}
