package be.kdg.distrib.testclasses;

public class TestImplementationWithStub {
    private final TestInterfaceWithAnnotation server;
    private TestInterface2 toSendAsRef;

    public TestImplementationWithStub(TestInterfaceWithAnnotation server, TestInterface2 toSendAsRef) {
        this.server = server;
        this.toSendAsRef = toSendAsRef;
    }

    public void sendAndDoStuffWithRef(){
        server.testCallByRef(toSendAsRef);
    }
    public void sendAndDoStuffWithRef(String text){
        server.testCallByRefWithParameters(toSendAsRef, text);
    }

    public String sendAndDoStuffWithRefWithReturn(String text){
        return server.testCallByRefWithReturn(toSendAsRef,text);
    }
}
