package be.kdg.distrib.testclasses;

import be.kdg.distrib.annotation.CallByRef;

public interface TestInterfaceWithAnnotation {
    void testCallByRef(@CallByRef TestInterface2 toRef);
    void testCallByRefWithParameters(@CallByRef TestInterface2 toRef, String text);
    String testCallByRefWithReturn(@CallByRef TestInterface2 toRef, String text);
}
