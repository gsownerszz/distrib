package be.kdg.distrib.testclasses;

public class TestRecursiveObject1 {
    private TestAnotherRecursiveObject testAnotherRecursiveObject;
    private int i;
    public TestRecursiveObject1(){

    }

    public void setTestRecursiveObject(TestAnotherRecursiveObject testRecursiveObject) {
        this.testAnotherRecursiveObject = testRecursiveObject;
    }

    public void setI(int i) {
        this.i = i;
    }

    public TestAnotherRecursiveObject getTestRecursiveObject() {
        return testAnotherRecursiveObject;
    }

    public int getI() {
        return i;
    }
}
