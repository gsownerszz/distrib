package be.kdg.distrib.testclasses;

public class TestImplementationWithAnnotationImpl implements TestInterfaceWithAnnotation {

    @Override
    public void testCallByRef(TestInterface2 toRef) {
        toRef.testMethod1();
    }

    @Override
    public void testCallByRefWithParameters(TestInterface2 toRef, String text) {
        toRef.testMethod2(text);
    }

    @Override
    public String testCallByRefWithReturn(TestInterface2 toRef, String text) {
        toRef.testMethod2(text);
        return toRef.getS();
    }

}
