package be.kdg.distrib.testclasses;

import be.kdg.distrib.annotation.IgnoreField;

import java.util.Objects;

public class TestObject {
    private String name;
    private int age;
    private char gender;
    private boolean deleted;
    @IgnoreField
    private float ignore;
    public TestObject() {
    }

    public TestObject(String name, int age, char gender, boolean deleted) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.deleted = deleted;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestObject that = (TestObject) o;
        return age == that.getAge() &&
                gender == that.getGender() &&
                deleted == that.isDeleted() &&
                name.equals(that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age, gender, deleted);
    }
}
