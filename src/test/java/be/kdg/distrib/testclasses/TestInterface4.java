package be.kdg.distrib.testclasses;

public interface TestInterface4 {
    void changeFields();
    void changeFields(int i);
    void changeFields(int i,boolean b);
    void changeFields(int i,boolean b, char c);
    void changeFields(int i,boolean b, char c,TestObject testObject);
    void trySameNumberOfParametersButOtherTypes(int i, boolean b);
    void trySameNumberOfParametersButOtherTypes(int i, TestObject testObject);
    void trySameNumberOfParametersButOtherTypes(int i, char c);
    void tryRecursiveParameterObjects(TestRecursiveObject1 recursiveObject);
    TestRecursiveObject1 tryRecursiveReturnType();

    void tryInterfaceAsParameter(TestInterface2 interface2);
}
