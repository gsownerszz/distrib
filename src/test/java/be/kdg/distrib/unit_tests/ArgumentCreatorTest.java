package be.kdg.distrib.unit_tests;

import be.kdg.distrib.annotation.IgnoreField;
import be.kdg.distrib.reflection_tools.ArgumentCreator;
import be.kdg.distrib.testclasses.TestObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
public class ArgumentCreatorTest {
    private ArgumentCreator argumentCreator;
    @Before
    public void setup() {
        argumentCreator = null;
        argumentCreator = new ArgumentCreator();
    }
    @Test()
    public void testCreateSimpleArgumentList() throws IllegalAccessException {
        String arg0 = "test";
        int arg1 = 123;
        char arg2 = 'F';
        boolean arg3 = true;
        double arg4 = 12.34;
        Class[] parameterTypes = new Class[]{arg0.getClass(), int.class, char.class, boolean.class, double.class};
        Object[] args = new Object[]{arg0, arg1, arg2, arg3, arg4};
        Map<String, String> argumentList = argumentCreator.createArgumentList(parameterTypes,args);
        Assert.assertEquals(arg0, argumentList.get("arg0"));
        Assert.assertEquals( Integer.toString(arg1), argumentList.get("arg1"));
        Assert.assertEquals(Character.toString(arg2), argumentList.get("arg2"));
        Assert.assertEquals( Boolean.toString(arg3), argumentList.get("arg3"));
        Assert.assertEquals( Double.toString(arg4), argumentList.get("arg4"));
    }
    @Test()
    public void testCreateObjectArgumentList() throws IllegalAccessException {
        TestObject testObject = new TestObject("Ownerszz", 21, 'm', true);
        Class[] parameterType = new Class[]{testObject.getClass()};
        Object[] args = new Object[]{testObject};
        Map<String, String> argumentList = argumentCreator.createArgumentList(parameterType,args);
        Assert.assertTrue(argumentList.keySet().stream().allMatch(key -> key.startsWith("arg0.")));
        Assert.assertEquals("Ownerszz", argumentList.get("arg0.name"));
        Assert.assertEquals("m", argumentList.get("arg0.gender"));
        Assert.assertEquals("21", argumentList.get("arg0.age"));
        Assert.assertEquals("true", argumentList.get("arg0.deleted"));
        assertTrue(!argumentList.containsKey("arg0.ignored"));
    }
    @Test()
    public void testCreateSimpleReturnList() throws IllegalAccessException {
        Map<String, String> returnList = new HashMap<>();
        argumentCreator.createResultList("test",returnList);
        assertEquals(1, returnList.keySet().size());
        assertEquals("test", returnList.get("result"));
    }
    @Test()
    public void testCreateObjectReturnList() throws IllegalAccessException {
        TestObject testObject = new TestObject("Ownerszz", 21, 'm', true);
        Map<String, String> returnList = new HashMap<>();
        argumentCreator.createResultList(testObject,returnList);
        int expectedFieldCount = (int) Arrays.stream(testObject.getClass().getDeclaredFields())
                .filter(field -> !field.isAnnotationPresent(IgnoreField.class))
                .count();
        assertEquals(expectedFieldCount, returnList.keySet().size());
        assertTrue(returnList.keySet().stream().allMatch(key -> key.startsWith("result.")));
        assertEquals(testObject.getName(), returnList.get("result.name"));
        assertEquals(Integer.toString(testObject.getAge()), returnList.get("result.age"));
        assertEquals(Character.toString(testObject.getGender()), returnList.get("result.gender"));
        assertEquals("true", returnList.get("result.deleted"));
    }
}
