package be.kdg.distrib.unit_tests;
import be.kdg.distrib.reflection_tools.ArgumentCreator;
import be.kdg.distrib.reflection_tools.Parser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
public class ParserTest {
    private Parser parser;
    @Before
    public void setup() {
        parser = null;
        parser = new Parser();
    }

    @Test
    public void testIfStringIsStringOrPrimitive(){
        String value = "test";
        Assert.assertTrue(parser.isStringOrPrimitive(value.getClass()));
        String parseResult = (String) parser.parseValue(value.getClass(), value.toString());
        Assert.assertEquals(value,parseResult);
    }
    @Test
    public void testIfBooleanIsStringOrPrimitive() {
        Boolean value = true;
        Assert.assertTrue(parser.isStringOrPrimitive(value.getClass()));
        Boolean parseResult = (Boolean) parser.parseValue(value.getClass(), value.toString());
        Assert.assertEquals(value,parseResult);
    }
    @Test
    public void testIfIntegerIsStringOrPrimitive() {
        Integer value = 123;
        Assert.assertTrue(parser.isStringOrPrimitive(value.getClass()));
        Integer parseResult = (Integer) parser.parseValue(value.getClass(), value.toString());
        Assert.assertEquals(value,parseResult);
    }
    @Test
    public void testIfFloatIsStringOrPrimitive() {
        Float value = 123.012f;
        Assert.assertTrue(parser.isStringOrPrimitive(value.getClass()));
        Float parseResult = (Float) parser.parseValue(value.getClass(), value.toString());
        Assert.assertEquals(value,parseResult);
    }
    @Test
    public void testIfLongIsStringOrPrimitive() {
        Long value = Long.MAX_VALUE;
        Assert.assertTrue(parser.isStringOrPrimitive(value.getClass()));
        Long parseResult = (Long) parser.parseValue(value.getClass(), value.toString());
        Assert.assertEquals(value,parseResult);
    }
    @Test
    public void testIfDoubleIsStringOrPrimitive() {
        Double value = 12.34;
        Assert.assertTrue(parser.isStringOrPrimitive(value.getClass()));
        Double parseResult = (Double) parser.parseValue(value.getClass(), value.toString());
        Assert.assertEquals(value,parseResult);
    }
    @Test
    public void testIfByteIsStringOrPrimitive() {
        Byte value = Byte.MAX_VALUE;
        Assert.assertTrue(parser.isStringOrPrimitive(value.getClass()));
        Byte parseResult = (Byte) parser.parseValue(value.getClass(), value.toString());
        Assert.assertEquals(value,parseResult);
    }
    @Test
    public void testIfShortIsStringOrPrimitive() {
        Short value = Short.MAX_VALUE;
        Assert.assertTrue(parser.isStringOrPrimitive(value.getClass()));
        Short parseResult = (Short) parser.parseValue(value.getClass(), value.toString());
        Assert.assertEquals(value,parseResult);
    }
}
