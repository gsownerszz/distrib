package be.kdg.distrib;
import be.kdg.distrib.skeletonFactory.Skeleton;
import be.kdg.distrib.skeletonFactory.SkeletonFactory;
import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;
import be.kdg.distrib.stubFactory.StubFactory;
import be.kdg.distrib.testclasses.*;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.*;

public class IntegrationTest {
    private Skeleton skeleton;
    private TestInterface2 stub;
    @Before
    public void setup() {
        //Create the implementation
        TestImplementation testImplementation = new TestImplementation();
        //Create the skeleton using the implementation as base
        skeleton = (Skeleton) SkeletonFactory.createSkeleton(testImplementation);
        //Create the stub using the interface that testImplementation implements
        stub = (TestInterface2) StubFactory.createStub(TestInterface2.class,skeleton.getAddress().getIpAddress(),skeleton.getAddress().getPortNumber());
        //Start the skeleton
        skeleton.run();
    }
    @Test
    public void testNoArgumentsNoReturnValue(){
         stub.testMethod1();
    }
    @Test
    public void testNoArgumentsWithSimpleReturnValue(){
        String returnValue = stub.testMethod5();
        assertNotNull(returnValue);
    }
    @Test
    public void testNoArgumentsWithComplexReturnValue(){
        TestObject returnValue = stub.testMethod11();
        assertNotNull(returnValue);
    }
    @Test
    public void testWithArgumentsWithNoReturnValue(){
        stub.testMethod3(0,"test",0.5,true,'o');
    }
    @Test
    public void testWithArgumentsWithSimpleReturnValue(){
        int i = 0;
        String test = "test";
        double v = 0.5;
        boolean b = true;
        char o = 'o';
        String returnValue =  stub.testWithArgumentsAndSimpleReturnValue(i,test,v,b,o);
        assertNotNull(returnValue);
        assertEquals(String.format("%d; %s; %f; %s; %s",i,test,v,b,o),returnValue);
    }
    @Test
    public void testWithArgumentsWithComplexReturnValue(){
        TestObject testObject = new TestObject("test",123,'m',false);
        TestObject returnValue =  stub.fullBlownTestMethod("Ownerszz",testObject,21,false);
        assertNotNull(returnValue);
        assertEquals("Ownerszz",returnValue.getName());
        assertEquals(21,returnValue.getAge());
        assertEquals('m',returnValue.getGender());
        assertTrue(returnValue.isDeleted());
    }

    @Test(expected = RuntimeException.class)
    public void testMethodNotFound(){
        TestInterface3 badInterface = (TestInterface3) StubFactory.createStub(TestInterface3.class,skeleton.getAddress().getIpAddress(),skeleton.getAddress().getPortNumber());
        badInterface.testMethod(0);
    }
    @Test()
    public void testRecursiveObjectAsParameter(){
        TestInterface4 impl = new TestInterFace4Impl();
        Skeleton skeleton = (Skeleton) SkeletonFactory.createSkeleton(impl);
        skeleton.run();
        TestInterface4 stub = (TestInterface4) StubFactory.createStub(TestInterface4.class,skeleton.getAddress().getIpAddress(),skeleton.getAddress().getPortNumber());
        TestRecursiveObject1 recursiveObject1 = new TestRecursiveObject1();
        recursiveObject1.setI(20);
        TestAnotherRecursiveObject anotherRecursiveObject = new TestAnotherRecursiveObject();
        anotherRecursiveObject.setTestObject(new TestObject("Ownerszz", 21, 'm', true));
        recursiveObject1.setTestRecursiveObject(anotherRecursiveObject);
        stub.tryRecursiveParameterObjects(recursiveObject1);
        assertEquals(new TestObject("Ownerszz", 21, 'm', true),recursiveObject1.getTestRecursiveObject().getTestObject());
        assertEquals(20,recursiveObject1.getI());
    }
    @Test()
    public void testRecursiveObjectAsReturnType(){
        TestInterface4 impl = new TestInterFace4Impl();
        Skeleton skeleton = (Skeleton) SkeletonFactory.createSkeleton(impl);
        skeleton.run();
        TestInterface4 stub = (TestInterface4) StubFactory.createStub(TestInterface4.class,skeleton.getAddress().getIpAddress(),skeleton.getAddress().getPortNumber());
        TestRecursiveObject1 recursiveObject1 = new TestRecursiveObject1();
        recursiveObject1.setI(20);
        TestAnotherRecursiveObject anotherRecursiveObject = new TestAnotherRecursiveObject();
        anotherRecursiveObject.setTestObject(new TestObject("Ownerszz", 21, 'm', true));
        recursiveObject1.setTestRecursiveObject(anotherRecursiveObject);
        stub.tryRecursiveParameterObjects(recursiveObject1);
        TestRecursiveObject1 returnValue = stub.tryRecursiveReturnType();
        assertEquals(new TestObject("Ownerszz", 21, 'm', true),returnValue.getTestRecursiveObject().getTestObject());
    }

    @Test()
    public void testCallByRef(){
        //Client side
        TestImplementation impl = new TestImplementation();
        Skeleton clientSkeleton = (Skeleton) SkeletonFactory.createSkeleton(impl);
        clientSkeleton.run();
        //Server side
        TestImplementationWithAnnotationImpl serverImpl = new TestImplementationWithAnnotationImpl();
        Skeleton  serverSkeleton = (Skeleton) SkeletonFactory.createSkeleton(serverImpl);
        serverSkeleton.run();
        //client side
        TestInterfaceWithAnnotation serverStub = (TestInterfaceWithAnnotation) StubFactory.createStub(
                TestInterfaceWithAnnotation.class,
                serverSkeleton.getAddress().getIpAddress(),
                serverSkeleton.getAddress().getPortNumber(),
                clientSkeleton.getAddress().getIpAddress(),
                clientSkeleton.getAddress().getPortNumber());
        TestImplementationWithStub testImplementationWithStub = new TestImplementationWithStub(serverStub,impl);
        testImplementationWithStub.sendAndDoStuffWithRef();
        assertEquals("void", impl.getS());
        String text = "testCallByRef";
        testImplementationWithStub.sendAndDoStuffWithRef(text);
        assertEquals(text, impl.getS());

        String newText = "Ownerszz";
        String result = testImplementationWithStub.sendAndDoStuffWithRefWithReturn(newText);
        assertEquals(newText, result);

    }

    @Test
    public void testNullAsParameter(){
        stub.testMethod4(null);
    }

    @Test
    public void testNullAsField(){
        TestInterface4 impl = new TestInterFace4Impl();
        Skeleton skeleton = (Skeleton) SkeletonFactory.createSkeleton(impl);
        skeleton.run();
        TestInterface4 stub = (TestInterface4) StubFactory.createStub(TestInterface4.class,skeleton.getAddress().getIpAddress(),skeleton.getAddress().getPortNumber());
        TestRecursiveObject1 recursiveObject1 = new TestRecursiveObject1();
        recursiveObject1.setI(20);
        stub.tryRecursiveParameterObjects(recursiveObject1);
    }

    @Test
    public void testInterfaceAsParameter(){
        TestInterface4 impl = new TestInterFace4Impl();
        Skeleton skeleton = (Skeleton) SkeletonFactory.createSkeleton(impl);
        skeleton.run();
        TestInterface4 stub = (TestInterface4) StubFactory.createStub(TestInterface4.class,skeleton.getAddress().getIpAddress(),skeleton.getAddress().getPortNumber());
        TestRecursiveObject1 recursiveObject1 = new TestRecursiveObject1();
        recursiveObject1.setI(20);
        stub.tryInterfaceAsParameter(new TestImplementation());
    }
}

