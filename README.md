See IntegrationTest for usage.

If you want to use this in your project using maven than add the following lines to the pom.xml OR if you want to change the Java version:
 ```
<build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.0</version>
                <configuration>
                    <release>12</release>
                </configuration>
            </plugin>
        </plugins>
    </build>
```
